package pcd.lab03.factorizer.cache_unsafe;

import pcd.lab03.factorizer.FactorizerService;
import pcd.lab03.factorizer.ServiceUser;

public class Test {

	public static void main(String[] args) {

		int nUsers = 2;
		FactorizerService service = new FactorizerWithCache(5);
		for (int i = 0; i < nUsers; i++){
			new ServiceUser(service,1000000).start();
		}
	}

}
